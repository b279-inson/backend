const Course = require("../models/Course");

// Create a new course
/*
// PREVIOUS CODE BEFORE ACTIVITY:
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})

	// Saves the created object to our DB
	return newCourse.save().then((course, error) => {
		if(error){
			return "There is an error saving this course";
		}else{
			return true;
		}
	})
}
*/

// ANSWER TO ACTIVITY:

// Create a new course
module.exports.addCourse = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}


// Retrieve/Get All Courses Function
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}



// Retrieve/Get All ACTIVE Courses Function
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a Specific Course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// Update a Specfic Course
module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}else{
				return "Course Updated!";
			}
		})
	}
	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}


// S40 ACTIVITY
// Archiving A Course

module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if (isAdmin) {
		let archivedCourse = {
			isActive : reqBody.isActive,
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error) {
				return false;
			}else{
				return true;
			}
		})
	}
	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};







