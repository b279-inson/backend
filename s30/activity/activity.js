
// Total number of fruits on sale
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "", fruitsOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// Total number of fruits with more than 20 stocks
db.fruits.aggregate([
  {$match: {stock: { $gte : 20 }}},
  {$group: {_id: "", enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// Average price of fruits onSale per supplier
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);


// Highest price of a fruit per supplier
db.fruits.aggregate([
  {$group: {_id: "$supplier_id", max_price: {$max:"$price"}}}
]);


// Lowest price of a fruit per suppier
db.fruits.aggregate([
  {$group: {_id: "$supplier_id", min_price: {$min:"$price"}}},
  {$sort: {total: 1}}
]);



