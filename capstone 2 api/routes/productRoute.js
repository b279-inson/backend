const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Get all products
/*
non-admin:

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})
*/

router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllProducts(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Get all "ACTIVE" products
/*
non-admin:

router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})*/


router.get("/", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllActive(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})



// Retrieve specific/single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})



// Update product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})



// Route to archiving a product (Admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});


// Route to activating a product (Admin only)
router.put("/:productId/activate", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});











// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;