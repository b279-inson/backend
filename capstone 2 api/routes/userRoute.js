const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")


// route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// route for user authentication/login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// route for non-admin user checkout (create order)

/*
router.post("/checkout", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})
*/


router.post("/checkout", auth.verify, (req, res,) => {
    let data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id,
        product : req.body
    }
        userController.checkout(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
router.post("/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});



// STRETCH GOAL:

// Route for setting user as admin 
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setAdmin(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});


// Get ALL ORDERS
router.get("/orders", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getOrders(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


/*
// Add to cart
router.post("/addToCart", auth.verify, async (req, res) => {
	const userId = auth.decode(req.headers.authorization);
	const orders = req.body.orders;

	if (userId.isAdmin) {
		res.send("Only customers are allowed to purchase.");
	}else{
		const result = await userController.addToCart(userId, orders);
		res.send(result);
	}
});

*/












// Allows us to export the "router" object that will be accessed in our index.js file.
module.exports = router;
