const Product = require("../models/Product");


// Function for CREATING a product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			brand: data.product.brand,
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}



// retrieve ALL products function
/*
non-admin:

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}
*/


module.exports.getAllProducts = (reqBody, isAdmin) => {
	if(isAdmin){
		return Product.find({}).then(result => {
		return result;
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	}) 
}



// retrieve all ACTIVE products function
/*
non-admin:

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}*/

module.exports.getAllActive = (reqBody, isAdmin) => {
	if(isAdmin){
		return Product.find({isActive : true}).then(result => {
		return result;
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	}) 
}

	


// Retreiving a SPECIFIC product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}



// UPDATE a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		brand : reqBody.brand,
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}



// ARCHIVING a product

module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateActiveField = {
		isActive : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {
				return false;
			// Product archived successfully
			} else {
				return true;
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};



// ACTIVATING a product

module.exports.activateProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateInactiveField = {
		isActive : true
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateInactiveField).then((product, error) => {

			// Product not activated
			if (error) {
				return false;
			// Product activated successfully
			} else {
				return true;
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};







