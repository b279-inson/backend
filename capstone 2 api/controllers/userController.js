const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")


// function to register a user

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		address: reqBody.address,
		// 10 is the value provided as the number of salt rounds.
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		}else{
			return true;
		}
	})

};



// function to login a user

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result == null) {
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}else{
				return "wrong password";
			}
		}			
	})

};



// function for non-admin user to checkout (create order)
/*
module.exports.userCheckout = async (data) => {

	if(data.isAdmin){
		return "ADMIN cannot do this action.";
	}else{

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orderedProduct.push({productId : data.productId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})

		if(isUserUpdated && isProductUpdated){
			return "You have successfully purchased the product!";
		}else{
			return "Something went wrong with your request. Please try again later!";
		}
	}
};
*/


module.exports.checkout = async (data, reqBody, isAdmin) => {
    console.log(data.isAdmin);

    if(data.isAdmin){
            return "You're not allowed to order!"
    }else{
        let isUserUpdated = await User.findById(data.userId).then(user => {
            return Product.findById(data.product.productId).then(result =>{
                console.log(result.name)
            let newOrder = {
                products : [{
                    productId : data.product.productId,
                    productName : result.name,
                    quantity : data.product.quantity
                }],
                totalAmount : result.price * data.product.quantity
            }
            user.orderedProduct.push(newOrder);


        console.log(data.product.productId);

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
            })

    })


    let isProductUpdated = await Product.findById(data.product.productId).then(product => {

        product.userOrders.push({userId: data.userId})

        return product.save().then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })

        })


    if(isUserUpdated && isProductUpdated){
        return "Sucessfully Ordered!";
    }else{
        return "Something went wrong with your request. Please try again later!";
    }
    }
}






// Retrieve user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};




// STRETCH GOAL:

// SETTING user as admin

module.exports.setAdmin = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateInactiveUser = {
		isAdmin : true
	};

	if(isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, updateInactiveUser).then((user, error) => {

			// user not archived
			if (error) {
				return false;
			// user archived successfully
			} else {
				return true;
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};



// Get ALL ORDERS
module.exports.getOrders = (reqBody, isAdmin) => {
	if(isAdmin){
		return OrderedProduct.find({}).then(result => {
		return result;
	})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	}) 
}




/*
// Add to Cart
module.exports.addToCart = async(userId, orders) => {
	let user = await User.findById(userId.id);

	let products = [];
	let totalAmount = 0;

	for (let i = 0; i < orders.length; i++) {
		let product = await Product.findById(orders[i].productId);

		const newProduct = {
			productId: product._id,
			brand: product.brand,
			name: product.name,
			price: product.price,
			quantity: orders[i].quantity
		}

		let subtotal = product.price * orders[i].quantity;

		user.orderedProduct.push({
			products: [newProduct],
			purchasedOn: new Date(),
			price: product.price,
			subtotal: subtotal,
			quantity: orders[i].quantity
		})

		totalAmount = totalAmount + subtotal;
		products.push(newProduct);
		product.userOrders.push({userId: userId.id});
		await product.save();
	}

	// Add totalAmount to the last order object in the orderedProduct array
	user.orderedProduct[user.orderedProduct.length - 1].totalAmount = totalAmount;

	let isUserUpdated = await user.save();

	if (isUserUpdated) {
		return "Purchased successful!";
	}else{
		return "Something went wrong with your request. Please try again later.";
	}
}


*/







