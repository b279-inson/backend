// console.log("Hello World!");


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let titleMap = title.map(dataToMap => dataToMap.title)
	console.log(titleMap);
});


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result));

// POST method
fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// PUT Method
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		id: 1,
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		id: 1,
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// DELETE Method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});


